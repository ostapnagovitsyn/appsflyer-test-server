to start app run 
- create and fill `.env` file in the root directory
- run `npm start:dev`

this app is following [12-factor app principles](https://12factor.net/)

 - [x] initial structure
 - [x] real gitlab calls
 - [ ] gitlab integration
 - [ ] repository entity for searching
 - [ ] unit and integration tests
 - [ ] code cleanup
 - [ ] Docker
 - [ ] improve README.md
