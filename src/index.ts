import dotenv from 'dotenv-safe'

dotenv.load()

import './lifecycleHandlers'
import { App } from './App'
import { settings } from './settings'

const app = new App(settings);

// Setup kernel signals stop handlers before start
// so everything will shutdown at least partially
['SIGTERM', 'SIGINT', 'SIGHUP']
  .forEach((sigEvent) => {
    process.on(sigEvent as NodeJS.Signals, () => app.stop())
  })

app.start()
