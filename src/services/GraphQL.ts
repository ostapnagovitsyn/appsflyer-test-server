import { ApolloServer, gql } from 'apollo-server'
import { Service } from '../../types'
import { $GitLab } from './GitLab'

export interface GraphQLSettings {
  port: number
}

export type $GraphQL = { graphql: GraphQL }

export class GraphQL implements Service {
  private server: ApolloServer

  constructor (private services: $GitLab, public settings: GraphQLSettings) {
    const typeDefs = gql`
        type Commit {
            id: String
            short_id: String
            title: String
            author_name: String
            author_email: String
            authored_date: String
            committer_name: String
            committer_email: String
            committed_date: String
            created_at: String
            message: String
            parent_ids: [String]
        }

        type Query {
            commits(page: Int = 1, perPage: Int = 20): [Commit]
            commit(sha: String): Commit
            searchProjects(search: String): [ProjectSearchResult]
        }

        type ProjectSearchResult {
            id: Int
            description: String
            name: String
            name_with_namespace: String
            path: String
            path_with_namespace: String
            created_at: String
            default_branch: String
            tag_list: [String]
            ssh_url_to_repo: String
            http_url_to_repo: String
            web_url: String
            avatar_url: String
            star_count: Int
            forks_count: Int
            last_activity_at: String
        }
    `

    const resolvers = {
      Query: {
        commits: (root, args) => this.services.gitlab.getCommits(args.page, args.perPage),
        commit: (root, args) => this.services.gitlab.getCommit(args.sha),
        searchProjects: (root, args) => this.services.gitlab.searchProjects(args.search),
      },
    }

    this.server = new ApolloServer({ typeDefs, resolvers })
  }

  async init () {
    const res = await this.server.listen(this.settings.port)
    console.log(`🚀  Server ready at ${res.url}`)
  }

  async stop () {
    await this.server.stop()
  }
}
