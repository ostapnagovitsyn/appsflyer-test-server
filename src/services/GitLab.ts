import axios, { AxiosInstance } from 'axios'
import { Service } from '../../types'

export interface GitLabSettings {
  baseUrl: string
  token: string
  project: string | number
}

export type $GitLab = { gitlab: GitLab }

export class GitLab implements Service {
  private instance: AxiosInstance
  project: string | number

  constructor (public settings: GitLabSettings) {}

  async init () {
    this.project = this.settings.project
    this.instance = axios.create({
      baseURL: this.settings.baseUrl,
      headers: {
        'Private-Token': this.settings.token,
      },
    })
  }

  /* tslint:disable-next-line */
  async stop () { }

  async getCommits (page: number, perpage: number) {
    const res = await this.instance.get(`/projects/${this.project}/repository/commits?page=${page}&per_page=${perpage}`)
    return res.data
  }

  async getCommit (sha: string) {
    const res = await this.instance.get(`/projects/${this.project}/repository/commits/${sha}`)
    return res.data
  }

  async searchProjects (search: string) {
    const res = await this.instance.get(`/search?scope=projects&search=${search}`)
    return res.data
  }
}
