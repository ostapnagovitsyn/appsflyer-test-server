import { createLogger, stdSerializers } from 'bunyan'
import { settings } from './settings'
const packagejson = require('../package.json')

const { name, version } = packagejson

export const logger = createLogger({
  name,
  version,
  level: settings.logLevel || 'debug',
  serializers: {
    error: stdSerializers.err,
  },
})
