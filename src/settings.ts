import querystring from 'querystring'

export const settings = {
  shutdownTimeout: 3000,
  env: process.env.NODE_ENV,
  logLevel: process.env.LOG_LEVEL,
  gitlab: {
    baseUrl: process.env.GITLAB_BASE_URL,
    token: process.env.GITLAB_TOKEN,
    project: querystring.escape(process.env.GITLAB_PROJECT),
  },
  graphql: {
    port: parseInt(process.env.GRAPHQL_PORT, 10),
  },
}

export type AppSettings = typeof settings
