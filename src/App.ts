import { AppSettings } from './settings'
import { $GitLab, GitLab } from './services/GitLab'
import { $GraphQL, GraphQL } from './services/GraphQL'
import { logger } from './logger'

type AppServices =
  $GitLab
  & $GraphQL

export class App {
  services: AppServices

  constructor (public settings: AppSettings) {
    const gitlab = new GitLab(this.settings.gitlab)
    this.services = {
      gitlab,
      graphql: new GraphQL({ gitlab }, this.settings.graphql),
    }
  }

  async start () {
    logger.info('Starting...')

    try {
      await this.services.gitlab.init()
      await this.services.graphql.init()
    } catch (error) {
      if (this.isTestEnv()) throw error
      logger.error(error, 'Error during startup')
      process.exit(1)
    }

    logger.info('Started')
  }

  async stop () {
    logger.info('Stopping...')

    // Last resort fallback to shutdown application no matter what
    const timeoutId = setTimeout(() => {
      if (this.isTestEnv()) throw new Error(`Failed to stop process gracefully`)
      logger.error(`Stopped forcefully`)
      process.exit(1)
    }, this.settings.shutdownTimeout)

    try {
      await this.services.graphql.stop()
      await this.services.gitlab.stop()
      clearTimeout(timeoutId)
      logger.info('Stopped')
    } catch (error) {
      if (this.isTestEnv()) throw error
      logger.error(error, 'Error during shutdown')
      process.exit(1)
    }
  }

  private isTestEnv () {
    return this.settings.env === 'test'
  }
}
