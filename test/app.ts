import { App } from '../src/App'
import { settings } from '../src/settings'

describe('app', function () {
  const app = new App(settings)

  it('should start and stop 1', async function () {
    await app.start()
    await app.stop()
  })

  it('should start and stop 2', async function () {
    await app.start()
    await app.stop()
  })
})
