require('dotenv-safe').load({ allowEmptyValues: true })

process.env.GITLAB_BASE_URL = 'http://test.url'
process.env.GITLAB_TOKEN = ''
process.env.GITLAB_PROJECT = 'project'
process.env.GRAPHQL_PORT = '4000'
process.env.LOG_LEVEL = 'debug'
