export interface Service {
  init (): Promise<void>

  stop (): Promise<void>
}
